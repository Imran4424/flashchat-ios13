//
//  Message.swift
//  Flash Chat iOS13
//
//  Created by Shah Md Imran Hossain on 4/8/21.
//  Copyright © 2021 Angela Yu. All rights reserved.
//

import Foundation

struct Message {
    let sender: String
    let body: String
}
